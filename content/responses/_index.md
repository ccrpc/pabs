---
title: "Responses and Findings"
draft: false
menu: main
weight: 40
abstract: A total of 550 questionnaires were completed in response to 1,631 questionnaires mailed to households in the MPA, yielding a response rate of 34 percent.
bannerHeading:
bannerText: >
    A total of 550 questionnaires were completed in response to 1,631 questionnaires mailed to households in the MPA, yielding a response rate of 34 percent.

---

This section presents the responses for each question on the questionnaire. A
total of 550 questionnaires were completed in response to 1,631 questionnaires
mailed to households in the MPA, yielding a response rate of 34 percent. The
minimum sample size was determined to be 384 responses and the minimum sample
size was achieved for all the questions except for the final four questions
which asked for respondents' favorite and least favorite walking and bicycling
routes. The number of responses for each question are shown in the figure below.

<rpc-chart url="allresponses.csv"
  x-label="Questions"
  y-label="Responses"
  legend=false
  chart-title="Number of Responses for Each PABS Question"
  source=" CCRPC"
  type="line"></rpc-chart>

## Questions about Recent Travel

{{<image src="weather.jpg"
  alt="Graphs presenting weather summaries for the months of April, May, June, and July of 2019"
  position="right"
  caption="Source: wunderground.com/history/monthly/us/il/savoy/KCMI/date/2019-4">}}

### Question 1. What is today's date?

The responses to this first question serve as a record of the time the responses
were logged and allows the data analysis to account for weather, holidays, and
seasons if necessary. The response dates ranged from April 8, 2019 through July
20, 2019 with most responses received in April. During this time,
there were no holidays or weather events that were deemed to be long enough or
extreme enough to significantly impact the weekly activities of the respondents.

<rpc-chart url="1.csv"
  x-label="Date"
  y-label="Responses"
  legend=false
  chart-title="1. Date of PABS Responses"
  source=" CCRPC"
  type="line"></rpc-chart>

### Question 2. Did you leave the Champaign-Urbana area during the last 7 days? If so, how many days?

This question identifies the respondents who may not have been in the MPA for
all or most of the previous seven days. Only 36 respondents were outside the
region for all seven days before they filled out the questionnaire. Since
questions 3-6 specifically ask about behavior in the past seven days, responses to
those questions were excluded from the 36 respondents who were out of the region
for that period of time. The removal of those responses is reflected in the
figure above entitled "Number of Responses for Each Question."

<rpc-chart url="2b.csv"
  y-min="0"
  x-label="Days"
  y-label="Percent"
  legend=false
  chart-title="2. Did you leave the Champaign-Urbana area during the last 7 days? If so, how many days?"
  source=" CCRPC"
  type="bar"></rpc-chart>

### Question 3. Tell us the most recent time you used each type of travel.

The responses to this question provide an overview of all the modes of
transportation the respondents use, even ones they only use occasionally. It is
easy to overlook modes of travel only used occasionally in questions that ask
only about travel in the past day or week.

According to the responses, walking is the most common form of transportation in
the MPA after driving or riding in vehicles. About 86 percent of questionnaire
respondents reported walking for recreation or exercise or to walk the dog in
the past year including 70 percent walking for those purposes in the past
week. Less than 14 percent reported not walking for recreation or exercise or to
walk a dog in the last year. Almost 66 percent of respondents reported walking
to or from another destination (i.e. job, store, park, friend’s house) in the
last year including 46 percent who did so in the last seven days. About 24 percent
of respondents walked to a bus stop in the past year, with 9 percent doing so in
the previous seven days.

Over 41 percent of questionnaire respondents reported bicycling for recreation
or exercise in the past year including almost 20 percent walking for the same
purpose in the past week. About 33 percent of respondents reported bicycling to
or from another destination (i.e. job, store, park, friend’s house) in the last
year including almost 17 percent who did so in the last seven days. Almost 7
percent of respondents walked to a bus stop in the past year, with almost 2
percent doing so in the previous seven days.

About 28 percent of respondents used transit in the last year including 7
percent who reported using transit in the past week. As expected, nearly
all respondents, 99 percent, had traveled in a vehicle as a driver or passenger
in the past year, including almost 96 percent who did so in the previous seven
days.

<rpc-chart url="3.csv"
  x-label="Percent"
  x-max=100
  stacked="true"
  chart-title="3. Tell us the most recent time you used each type of travel"
  source=" CCRPC"
  type="horizontalBar"></rpc-chart>

As discussed in the [Background](ccrpc.gitlab.io/pabs/summary/) section, the
main source of data currently available for mode use in the MPA is the American
Community Survey (ACS). The ACS asks the following question:

>[*How did this person usually get to work LAST WEEK? If this person usually
>used more than one method of transportation during the trip, mark (X) the box
>of the one used for most of the
>distance*](https://www2.census.gov/programs-surveys/acs/about/qbyqfact/2016/JourneytoWork.pdf).

This question is worded to capture only the most dominant form of transportation
used to get to work and exclude occasional and season travel as well as the less
dominate modes in multimodal trips (i.e. walking or bicycling to or from a bus
stop), which has the result of underrepresenting the less common or less frequent
commuting modes of walking and bicycling. In addition, responses to the PABS
questionnaire show consistently higher walking and bicycling rates for
recreation than to get to destinations including jobs (21 percent higher for
walking and 8 percent higher for bicycling over the past year), which is also
not captured by the ACS. As a result, relying exclusively on the ACS commuting
data results in under-representing walking and biking in the local
transportation network.

It is not particularly useful or appropriate to directly compare the ACS
estimates with the PABS responses since these are two different surveys with
considerably different methodologies, sample populations, and questions.
However, when considered together, they can help transportation planners paint a
more holistic picture of transportation mode use in the MPA.

## Questions about Frequency of Bicycling and Walking

### Question 4. In the last 7 days, on how many days did you bicycle to a bus stop, to work or school, to some other destination, and/or for exercise or recreation?

This question provides information on the frequency and purpose of bicycling
trips over the last seven days. Asking about travel behavior for a short, recent
time period is standard procedure in travel behavior research. The figure below
illustrates the respondents' bicycle travel to or from bus stops, to or from work
or school, to other destinations, and for exercise or recreation in the previous
seven days. Respondents reported bicycling more for exercise or recreation than
the other three purposes, mirroring the responses regarding bicycle behavior in
question three.

About 24 percent of respondents biked for exercise or recreation in the past
seven days, with almost 4 percent doing so at least five days. About 18 percent
of respondents biked to or from a destination other than work, school, or a bus
stop in the past seven days, with 3 percent doing so at least five out of the
last seven days. Almost 12 percent of respondents biked to or from work or
school in the past seven days, with almost 4 percent doing so at least five
days, which could mean biking was their main or only form of transportation to or
from work or school in the past seven days. About 2 percent of respondents biked
to or from a bus stop in the past seven days, with about 1 percent doing so at
least five days.

  <rpc-chart url="4.csv"
    chart-title="4. In the last 7 days (up to yesterday), on how many days did you:"
    x-label="Percent"
    stacked="true"
    x-max="100"
    source=" CCRPC"
    type="horizontalBar"></rpc-chart>

### Question 5. In the last 7 days, on how many days did you walk to a bus stop, to work or school, to some other destination, and/or for exercise or recreation?

This question asks about the frequency of walking to the same destinations asked
about for bicycling in the previous question. Almost 72 percent of respondents
walked for exercise or recreation in the past seven days, with over 25 percent
doing so at least five out of the last seven days. About 48 percent of
respondents walked to or from a destination other than work, school, or a bus
stop in the past seven days, with almost 11 percent doing so at least five days.
Almost 11 percent of respondents walked to or from work or school in the past
seven days, with over 4 percent doing so at least five days, which could mean
walking was their main or only form of transportation to or from work or school
in the past seven days. About 12 percent of respondents walked to or from a bus
stop in the past seven days, with over 2 percent doing so at least five days.

  <rpc-chart url="5.csv"
    chart-title="5. In the last 7 days (up to yesterday), on how many days did you:"
    x-label="Percent"
    stacked="true"
    x-max="100"
    source=" CCRPC"
    type="horizontalBar"></rpc-chart>

## Questions about General Travel

### Question 6. In the last 7 days, did you have access to a working motor vehicle and/or a working bicycle?

This question provides information about respondents' access to motor vehicles
and bicycles to provide additional information relevant to mode choices.
Responses show that the vast majority of respondents (almost 96 percent) had
access to a working motor vehicle all or most of the time in the past seven
days, not including taxis or ride hailing services such as Uber or Lyft. Only
about 3 percent of respondents reported never having access to a motor vehicle
in the past seven days.

Only about half of respondents (just under 54 percent) reported having access to
a working bicycle in the previous seven days. Over 40 percent reported never
having access to a working bicycle during that time.  

<rpc-chart url="6.csv"
  chart-title="6. In the last 7 days, did you have access to the following forms of transportation?"
  x-label="Percent"
  stacked="true"
  source=" CCRPC"
  type="horizontalBar"></rpc-chart>

The ACS asks a similar question about vehicle ownership that can provide a point
of comparison to assess how representative the PABS sample population is in this
regard. This question can be compared with ACS data on vehicle ownership. The
ACS asks the following question:

  >[*How many automobiles, vans, and trucks of one-ton capacity or less are kept
  >at home for use by members of this
  >household?*](https://www.census.gov/acs/www/about/why-we-ask-each-question/vehicles/).

According to the ACS, about 13 percent, rather than 3 percent, of the MPA
population is without regular access to a vehicle. Although the questions in the
ACS and the PABS are worded differently, the 10 percent difference between the
two data sets indicate the PABS sample population is likely underrepresenting
persons without access to a working motor vehicle. As a result, the PABS sample
population might also be underrepresenting persons who rely on non-vehicle modes
of transportation such as walking, bicycling, and transit.

### Question 7. Do you currently have any physical or other health conditions that limit the amount of bicycling or walking you can do?

This question seeks to identify what proportion of the sample population may
have limited transportation choices due to physical or health conditions,
regardless of other limitations such as weather or access to walking or
bicycling infrastructure. About 20 percent of respondents reported having a
physical or health condition that limits their walking. Question number three
told us that less than 14 percent of respondents did not walk for recreation,
exercise, or to walk a dog in the past year, suggesting that most or all
respondents who are able to walk, walked for recreation or exercise in the past
year including some respondents with health conditions that limit walking.

About 25 percent of respondents reported having a physical or health condition
preventing them from using a bicycle. According to question number three, over
41 percent of respondents bicycled for recreation or exercise in the past year
while 59 percent did not. When subtracting the 25 percent of respondents who
reported being unable to bicycle due to a physical or health condition, that
leaves only 34 percent of respondents who were able to bicycle but chose not to
in the past year.

The high percentage of people with physical or health conditions that prevent
them from walking or bicycling can at least partially be attributed to the older
average age of the survey sample population compared with the overall MPA
population. In the overall MPA population, residents age 60 and older account
for approximately 15 percent of the total population. In the PABS sample
population, residents age 60 and over account for approximately 53 percent of
the PABS sample population and approximately 80 percent of all the reported
physical or health conditions that limit the amount of walking and bicycling.
Respondents were asked to provide their age in question 13, discussed in the
[Respondents](https://ccrpc.gitlab.io/pabs/profile/) section.

<rpc-chart url="7.csv"
  x-label="Percent"
  chart-title="7. Do you currently have any physical or other health conditions that limit the amount of bicycling or walking you can do?"
  stacked="true"
  source=" CCRPC"
  type="horizontalBar"></rpc-chart>  

### Question 8. During a typical week, how many days does your commute to work or school include any of the following forms of transportation?

This question asks respondents about transportation modes used specifically for
commuting to work or school in the previous seven days, given that these are the
most common and consistent trips made by most households in the region.  For
these trips, the number of days varies significantly for each mode, but some
patterns emerge when comparing the number of days each mode was used with the
average number of days each mode was utilized by all the respondents combined.

Walking was used at least one day during the previous seven days by almost 19
percent of respondents, including almost 4 percent that walked to work or school
all seven days. On average each respondent walked an average of 0.75 days.
Bicycling was used at least one day during the previous seven days by just over
13 percent of respondents, with most respondents bicycling 4 days or less for an
average of 0.43 days for each respondent on average. Transit was used by just
over seven percent of the respondents to get the school or work in the previous
seven days. On average transit was used to commute 0.17 days by all respondents
during the previous week. Over 95 percent of respondents drove alone or as a
passenger in a vehicle to get to work or school at least one out of the previous
seven days. Most vehicle commuters (71 percent) drove alone at least one day,
with most driving 5 or more out of the previous seven days for an average of
3.71 days for all respondents. About 25 percent of respondents rode as a
passenger in a vehicle for one or more days with an average of 0.73 days
overall.

<rpc-chart url="8.csv"
  x-label="Percent"
  chart-title="8. During a typical week, how many days does your commute to work or school include any of the following forms of transportation?"
  x-max="100"
  stacked="true"
  source=" CCRPC"
  type="horizontalBar"></rpc-chart>

### Question 9. How many months in a year do you typically not make trips by bicycle or walking because of local climate/bad weather?

This question attempts to account for the impacts of climate and weather on
walking and bicycling behavior. This issue is significant for a region in the
Midwest with four seasons including annual snowfall. Respondents were asked how
many months out of the year they would avoid taking walking and bicycling trips.
About 37 percent of respondents reported bad weather would not prevent them from
walking and about 34 percent would not be prevented from bicycling. On average,
respondents reported choosing not to walk an average of four months out of the
year and choosing not to bicycle an average of five months out of the year.

<rpc-chart url="9.csv"
  chart-title="9. How many months in a year do you typically not make trips by bicycle or walking because of bad weather?"
  x-label="Percent"
  stacked="false"
  source=" CCRPC"
  type="horizontalBar"></rpc-chart>

## Favorite Walking and Bicycling Routes

### Questions 21 and 22. Favorite and Least Favorite Walking and Bicycling Routes

The final questions requested favorite and least favorite walking and bicycling
routes. The map below includes over 400 of the responses and the table below the
map lists the rest of the responses (with duplicates removed) that didn't have
enough information to be mapped.

<iframe src="https://maps.ccrpc.org/routes-2019-pabs/" width="100%" height="600" allowfullscreen="true"></iframe>

<rpc-table
url="not_mapped.csv"
table-title="Not Mapped: PABS Responses to Favorite and Least Favorite Walking and Bicycling Routes"
source=" CCRPC"
</rpc-table>
