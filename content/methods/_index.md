---
title: "Methodology"
draft: false
menu: main
weight: 20
abstract: Four separate contacts were developed in an attempt to increase the number of survey responses.
bannerHeading:
bannerText: >
    Four separate contacts were developed in an attempt to increase the number of survey responses.

---

The Pedestrian and Bicycle Survey (PABS) for the Champaign-Urbana MPA took a
well-documented methodological approach, relying on mailing out printed survey
materials with both a mail back and an online option for responding. This
approach was selected based on its reliability, affordability, and feasibility.
CUUATS staff relied on previous experience administering local surveys as well
as the Mineta Transportation Institute and the book *Mail and Internet Surveys:
The Tailored Design Method*, by Don Dillman, for guidance on designing and
administering the survey.

## Questionnaire

The questions included in the 2019 Champaign-Urbana PABS questionnaire are based
on the [Mineta Institute’s Pedestrian and Bicycle Survey
(PABS)](https://transweb.sjsu.edu/research/measuring-walking-and-cycling-using-pabs-pedestrian-and-bicycling-survey-approach-low-cost),
one of very few established surveys that collects walking and cycling data for
transportation planning purposes with questions tested for reliability. While a
PABS has never been administered for the MPA, CUUATS staff administered a PABS
for the City of Urbana in 2013 and 2014 in the course of updating [Urbana’s
Bicycle Master
Plan](https://www.urbanaillinois.us/sites/default/files/attachments/2016_Urbana_Bicycle_Master_Plan_0.pdf).
The MPA PABS questionnaire consists of 22 questions which can be found below and
in the [Respondents](https://ccrpc.gitlab.io/pabs/profile/) and [Responses and
Findings](https://ccrpc.gitlab.io/pabs/responses/) sections. Questions 1-20
closely match the original Mineta questions that were tested for reliability,
with only minor modifications. Questions 21 and 22 were added to capture
opinions about walking and bicycling routes in the Champaign-Urbana region. The
questions are divided into the following six sections:

1.	Questions about your recent travel (questions 1-3)
2.	Questions about how often you bicycled or walked in the last 7 days (questions 4-5)
3.	Questions about your general travel (questions 6-9)
4.	Questions about you (questions 10-16)
5.	Questions about your household (questions 17-20)
6.	Comments (questions 21-22)

The paper version of the questionnaire is below. The questionnaire was also made
available as a Google Form. Visit the
[Appendix](https://ccrpc.gitlab.io/pabs/appendix/) to see the questionnaire
translated into Spanish, Chinese, and French.

{{<image src="questionnaire_en.jpg"
  alt="Paper version of the 2019 C-U PABS questionnaire in English"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  position="full">}}

{{<image src="table_samplesizes.jpg"
  alt="Table of completed sample sizes needed for various population sizes and characteristics at three levels of precision"
  position="right"
  caption="Source: Dillman, Don A. (2007). Mail and Internet Surveys: The Tailored Design Method. (2nd ed.). Hoboken, NJ: Wiley.">}}

## Sample Population

The population of the MPA was approximately 162,499 people in 2017 according the
American Community Survey (the most recent population data available in 2019).
Without enough resources to send questionnaires to every individual in the
entire MPA, a simple random sample was drawn from a list of all the addresses
within the MPA obtained from the Champaign County Tax Assessor. Random samples
are intended to generate results that can be generalized to a larger population.
Sample populations can yield a set of responses that provide good estimates of
community-wide behavior, but it is important to remember that the results are
only estimates and not universal truth. CUUATS staff referred to the *Table:
Sample Sizes*, taken from the aforementioned book *Mail and Internet Surveys*,
to calculate the size of the survey sample population considering the MPA
population, sampling error, and confidence level. According to the table, 384
questionnaires should be completed in order for the responses to be
representative with a 95% confidence level and a +/- 5% margin of error. Based
on research, previous experience, and budget constraints, staff calculated that
a sample of 1,600 individuals should yield at least 384 returned questionnaires
based on a 24% response rate.

## Survey Implementation

Four separate contacts were developed in an attempt to increase the number of
responses. Research shows that multiple contacts are among the most effective
techniques for increasing response to questionnaires by mail. The four contacts
were mailed out over about eight weeks in April and May of 2019. Each of the
four contacts is described below.

{{<image src="timeline.jpg"
  alt="Timeline of mailed survey components over 8 weeks"
  position="full">}}

{{<image src="postcard_horizontal.jpg"
  alt="The front and back of the 2019 C-U PABS advance postcard"
  position="right">}}

### Advance Postcard

The intent of the advance postcard was to let the sample population know that
the questionnaire would arrive soon, so they could look for it in the mail. The
postcard included basic information about the Champaign-Urbana PABS in English,
Spanish, French, and Chinese. The name and logo of the Champaign County Regional
Planning Commission (CCRPC) was used on the postcard to serve as a familiar agency to
give credibility to the survey. Information about the survey was included on
the CCRPC website in case any postcard recipients wanted more information. This
postcard also served as an affordable way to improve the sample population
address list by removing addresses from which postcards were returned as
undeliverable.

{{<image src="coverletter1.jpg"
  alt="The front and back of the 2019 C-U PABS cover letter"
  position="right">}}

### Questionnaire with Cover Letter and Pre-Paid Return Envelope

As previously mentioned, the questionnaire itself was closely based on the
Mineta PABS, with minor wording and formatting adjustments for regional
relevance and space. The questions were also translated into Spanish, French,
and Chinese and formatted as a Google Form in all four languages for those who
preferred to complete the questionnaire online. A cover letter was included with
the questionnaire to introduce recipients to the PABS project. The cover letter
was printed on CCRPC/CUUATS letterhead, signed by the CCRPC CEO, and included
information on the reasons for the survey, basic instructions, and links to the
English, Spanish, French, and Chinese versions online as well as how to request
a paper copy in Spanish, French, or Chinese. A pre-paid return envelope was also
included to make it easier for respondents to return the questionnaire.

{{<image src="postcard2_horizontal.jpg"
  alt="The front and back of the 2019 C-U PABS thank you/reminder postcard"
  position="right">}}

### Thank you/reminder postcard

The intent of the thank you/reminder postcard was twofold: to thank respondents
that had already completed and returned the questionnaire and to remind others
to please do so. The postcard included basic information about the PABS in
English, Spanish, French, and Chinese. Again, the name and logo of the CCRPC was
used on the postcard to serve as a familiar agency to give credibility to the
survey. Information about the survey was included on the CCRPC website in case
any postcard recipients wanted more information. The postcard also included the
links to the English, Spanish, French, and Chinese versions of the questionnaire
online.

{{<image src="coverletter2.jpg"
  alt="The front and back of the 2019 C-U PABS replacement cover letter"
  position="right">}}

### Replacement Questionnaire with Cover Letter and Pre-Paid Envelope

A second printed copy of the questionnaire was mailed, along with another cover
letter and pre-paid return envelope as the final contact with the sample
population. A new cover letter was written to remind recipients about the survey
and let them know they will not be contacted again. Again, the cover letter was
printed on CCRPC/CUUATS letterhead, signed by the CCRPC CEO, and included
information on the reasons for the survey, basic instructions, and links to the
English, Spanish, French, and Chinese versions of the questionnaire online as
well as how to request a paper copy in Spanish, French, or Chinese.
