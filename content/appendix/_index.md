---
title: "Appendix"
draft: false
menu: main
weight: 50
abstract: The 2019 PABS questionnaire was translated into Spanish, Chinese, and French.
bannerHeading:
bannerText: >
    The 2019 PABS questionnaire was translated into Spanish, Chinese, and French.

---

The paper versions of the Spanish, Chinese, and French questionnaires are
included below. All questionnaires (English, Spanish, Chinese, and French) were
also available to complete as Google Forms for the duration of the survey.

## Spanish Questionnaire ##

{{<image src="questionnaire_es.jpg"
  alt="Paper version of the 2019 C-U PABS questionnaire in Spanish"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  position="full">}}

## Chinese Questionnaire ##

{{<image src="questionnaire_ch.jpg"
  alt="Paper version of the 2019 C-U PABS questionnaire in Chinese"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  position="full">}}

## French Questionnaire ##

{{<image src="questionnaire_fr.jpg"
  alt="Paper version of the 2019 C-U PABS questionnaire in French"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  position="full">}}
