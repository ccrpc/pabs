---
title: "Home"
draft: false
bannerHeading: 2019 Champaign-Urbana Pedestrian and Bicycle Survey Report
bannerText: >

---

The Champaign-Urbana Metropolitan Planning Area (MPA) Pedestrian and Bicycle
Survey (PABS) was administered in 2019 by the Champaign-Urbana Urbanized Area
Transportation Study (CUUATS). This survey was carried out with financial
support from the Illinois Department of Transportation (IDOT) as part of a
larger grant to expand local efforts to collect and monitor active
transportation activity and infrastructure in the region.

The 2019 PABS survey covered the following topics:

- General purposes of walking and bicycling trips
- Frequency of walking and bicycling
- Reasons for not walking or bicycling
- Favorite and least favorite bicycling and walking routes in the community

A total of 550 questionnaires were completed in response to 1,631 questionnaires
mailed to households in the Champaign-Urbana metropolitan planning area in 2019,
yielding a response rate of 34 percent. This website serves as the documentation
for the survey including the background of the survey, the methods used to carry
out the survey, a profile of the questionnaire respondents, and a
summary of the questionnaire responses that can inform future transportation
system investments.
