---
title: "Background"
draft: false
menu: main
weight: 10
abstract: The Champaign-Urbana region is known for high rates of walking and bicycling compared to state and national averages.
bannerHeading:
bannerText: >
    The Champaign-Urbana region is known for high rates of walking and bicycling compared to state and national averages.

---

{{<image src="CU_Urbanized_map4.jpg"
  alt="Map of the Champaign-Urbana metropolitan planning area (MPA) boundary including major highways and the municipalities of Champaign, Urbana, Savoy, Tolono, Mahomet, and Bondville"
  position="right"
  caption="Champaign-Urbana MPA and municipal boundaries"
  attr=" CUUATS"
  attrlink="https://ccrpc.org/" >}}

The 2019 Pedestrian and Bike Survey (PABS) was administered for the
Champaign-Urbana metropolitan planning area (MPA) which includes the Cities of
Champaign and Urbana as well as the Villages of Savoy, Tolono, Bondville, and
Mahomet. The MPA boundary is agreed upon by the Long Range Transportation Plan
Steering Committee every five years during the LRTP planning process and
approved by the CUUATS Technical and Policy Committees. According to Federal
regulations, the MPA boundary must cover at least the existing urbanized area
and the contiguous area expected to become urbanized during the Long Range
Transportation Plan 20-year forecast period. This boundary is used to determine
what projects are to be included in the Transportation Improvement Program.

The Champaign-Urbana region is known for high rates of walking and bicycling
compared to state and national averages (see chart below). The City of Urbana,
City of Champaign, and University of Illinois have each been recognized for
their level of service provided to bicyclists and commitment to walking and
bicycling as sustainable forms of transportation. The Champaign-Urbana Mass
Transit District also supports walking and biking in the community by allowing
walkers and bicyclists to combine walking and biking trips with transit trips to
reach farther destinations. All CUMTD buses are ADA accessible and equipped with
bike racks. Walking and bicycling provide several advantages to individuals and
the larger community compared to driving automobiles. Walking and biking offer
physical exercise and while avoiding the pollution, roadway degradation, and
various expenses associated with vehicle usage and ownership. Despite the high
rates of walking and bicycling, very little data is collected on when and where
people in the community walk and bike.

Currently, the most consistent source of transportation mode use data for the community is from
the American Community survey (ACS), a product of the Census Bureau and the
leading source of population and economic data in the United States. The ACS is
an ongoing survey that provides a wide range of critical and reliable data for a
wide range of purposes. The limitation of the [ACS data related to
transportation mode
use](https://www2.census.gov/programs-surveys/acs/about/qbyqfact/2016/JourneytoWork.pdf)
is that it asks exclusively about the dominant means of transportation used to
get to work, which excludes transportation modes that are only sometimes used to
commute to work or modes that are used for other purposes or to access other
destinations.

<rpc-chart
url="acs-transportationwork2.csv"
type="bar"
stacked="true"
switch="false"
y-label="Percent of Workers Over 16 Years"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
chart-title="Commuting Mode for Workers 16 Years and Older, 2017"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table S0801"
</rpc-chart>

To fund efforts to collect more data on walking and bicycling in the region,
CUUATS staff applied for, and was awarded, a grant from the Illinois Department
of Transportation (IDOT) to support transportation data collection efforts in
Champaign-Urbana between 2018 and 2020. Among other planning tasks, the grant
included two separate initiatives specific to pedestrian and bicycle data
collection. The first initiative was to administer a survey to collect
information on pedestrian and bicycle behavior in the MPA, which is the subject
of this report. The second initiative was to collect [pedestrian
counts](https://maps.ccrpc.org/2019-pedestrian-counts/) and [bicycle
counts](https://maps.ccrpc.org/2019-bike-counts/) at over 20 different locations
around the community that have been identified as main access or entry points to
significant community locations or amenities. The counts were collected by
installing temporary video cameras to get 12-hour traffic counts at each
location. The grant also covered the purchase of six permanent pedestrian and
bicycle traffic counters that will be installed at three locations (two counters
at each location) which will collect pedestrian and bicycle traffic counts
around the clock after they are installed. The survey results and traffic counts
provide much needed bike and pedestrian data for the community to support
data-driven transportation planning and complement other pedestrian and bicycle
data collection efforts already in place such as the [sidewalk network
inventory](https://ccrpc.gitlab.io/sidewalk-explorer/), the
[BikeMoves](https://bikemoves.me/) application, and [other traffic
counts](https://data.ccrpc.org/dataset/traffic_counts).
