---
title: "Respondents"
draft: false
menu: main
weight: 30
abstract: Respondent demographics are compared with MPA demographics to evaluate how well the respondent population represents the overall MPA population.  
bannerHeading:
bannerText: >
    Respondent demographics are compared with MPA demographics to evaluate how well the respondent population represents the overall MPA population.

---

Eleven of the PABS questionnaire questions (questions 10-20) solicit
non-transportation information about the respondent and their household to
evaluate how well the respondent population reflects the overall MPA population.
A summary of PABS respondent data compared with MPA estimates from the American
Community Survey (ACS) is presented below, followed by a summary of each
individual question and its responses. It is important to consider population
characteristics that differ substantially between the PABS respondents and the
overall MPA when processing the PABS responses regarding transportation
behavior. Any significant differences should inform the application of the
survey findings as well as the methods used to administer future surveys in the
region.

- The average age of PABS respondents is approximately 18 years older than the
  average age of the MPA population. Age can influence transportation choices in
  a variety of ways including the destinations people travel to, the amount of
  time and money people have to spend on transportation, the ability to obtain a
  driver's license, and more. As a college town, college-age students are
  particularly underrepresented in this survey.

- People who identify as white or Caucasian are significantly overrepresented
  and people who identify as any other racial or ethnic group are significantly
  underrepresented among respondents. In addition, despite spending time and
  resources to translate all survey materials into Spanish, Chinese, and French,
  only one respondent submitted their PABS questionnaire in a language other
  than English despite about 8 percent of MPA residents having limited English
  proficiency.

- While it is difficult to determine exact numbers based on the information
  collected, the income of survey respondents appears to be significantly
  higher, on average, than that of the overall MPA population. Income can have
  large influence over transportation mode choices as well as residential,
  employment, and educational choices that impact transportation patterns. Low
  income individuals are more likely than those with above-average incomes to
  rely on the pedestrian, bicycle, and transit networks for transportation. By
  underrepresenting low income individuals and households, this survey
  underrepresents an important faction of pedestrian and bicycle network users.

- PABS respondents are more likely to have access to a working vehicle than the
  overall MPA population. As with low income individuals, people without access
  to a working vehicle are more dependent on the pedestrian, bicycle, and
  transit networks for transportation. By underrepresenting residents without
  access to a vehicle, this survey underrepresents a critical group of
  pedestrian and bicycle network users.

<rpc-table
url="profile_summary.csv"
table-title="PABS Respondent Profile Summary"
source=" CCRPC; U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates"
</rpc-table>

## Questions about the Survey Respondent

### Question 10. What two streets intersect closest to your home?

Responses to question 10 allowed planners to geolocate the nearest intersection
for each respondent in order to make sure the respondents were distributed
throughout the MPA. The locations of survey respondents will not be published in
order to preserve anonymity. The Zip Codes provided in
question 11 serve as the public record of the geographic distribution of the
respondents.

### Question 11. What Zip Code do you live in?

The Villages of Savoy and Mahomet were slightly overrepresented among the
questionnaire respondents while the Cities of Champaign and Urbana were slightly
underrepresented. No questionnaire respondents reported living in the Village of
Bondville, which accounts for approximately 0.3 percent of the MPA population.

<rpc-chart url="11.csv"
  chart-title="11. What Zip Code do You Live In?"
  x-label="People"
  y-label="Municipality"
  source=" CCRPC; U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table S0101"
  type="horizontalBar"></rpc-chart>

### Question 12. How many years have you lived in your neighborhood?

Over a third of respondents had lived in their neighborhood for over 20 years.
This trend aligns with the higher average age of respondents compared to the MPA
population. Only about 5 percent of respondents had lived in their neighborhood
for less than a year. If a much larger proportion of respondents had lived in their
neighborhood for less than a year, it is possible their frequency of walking and
bicycling could be partially due to a lack of familiarity with the local
destinations and/or pedestrian and bicycle networks, however that is not a
concern here.

<rpc-chart url="12.csv"
  chart-title="12. How many years have you lived in your neighborhood?"
  source=" CCRPC"
  type="pie"></rpc-chart>

### Question 13. What is your age?

The survey respondents’ ages skew significantly older than the overall MPA
population. The presence of the University of Illinois flagship campus results
in an above-average proportion of residents between the ages of 18 and 29 in the
MPA compared with the state and nation overall. Residents ages 18 to 29 make up
approximately 44 percent of the MPA population over the age of 18 according to
the ACS, but only about five percent of the PABS respondents (who are all 18
years old and over). In addition, the proportion of residents age 60 and older
make up about 52 percent of the respondent population but only about 18 percent
of MPA population over the age of 18.

<rpc-chart url="13.csv"
  chart-title="13. What is your age?"
  x-label="People"
  y-label="Age"
  stacked="false"
  source=" CCRPC; U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table S0101"
  type="horizontalBar"></rpc-chart>

### Question 14. What best describes your gender?

A higher proportion of female residents make up the respondent population compared
to the overall MPA population. About 2 percent of respondents reported being
non-binary or preferred not to select a gender. The ACS only produces estimates
for female and male gender categories.

<rpc-chart url="14.csv"
  chart-title="14. What best describes your gender?"
  x-label="People"
  stacked="false"
  source=" CCRPC; U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table S0101"
  type="horizontalBar"></rpc-chart>

### Question 15. Which racial/ethnic group(s) do you most identify with?

A much higher proportion of the survey respondents identify as Caucasian (about
90 percent) compared with the MPA population overall (about 71 percent). In
return, smaller proportions of respondents identifying as Asian,
African-American, and Hispanic/Latino answered the survey when compared to the
proportions of those groups in the overall MPA population.

<rpc-chart url="15.csv"
  chart-title="15. Which racial/ethnic group(s) do you most identify with?"
  x-label="People"
  stacked="false"
  source=" CCRPC; U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table DP05"
  type="horizontalBar"></rpc-chart>

### Question 16. Which occupational categories best describe you?

Respondents had multiple options to select from to identify their occupational
status. For example, respondents could be simultaneously going to school and
working for pay outside of the home. The chart below displays all responses. The
two most common answers were "Working for pay outside the home" followed by
"Retired." Over half of the respondents (52 percent) reported working for pay
outside the home though that option accounts for only about 46 percent of all
responses since respondents could select more than one answer to this question.
About 39 percent of respondents reported being retired though that option
accounts for just under 35 percent of total responses. The high percentage of
retired respondents corresponds with the higher average age of the residents who
responded to the questionnaire.

<rpc-chart url="16.csv"
  chart-title="16. Which category(ies) best describes you? (Check all that apply)"
  source=" CCRPC"
  type="pie"></rpc-chart>

## Questions about the Respondent's Household

### Question 17. How many people live in your household?

The percent of one-person households among respondents is significantly lower
than the percent of one-person households in the MPA overall. This aligns with
college-age students being significantly underrepresented in the respondent
population as discussed in question 13.

<rpc-chart url="17.csv"
  chart-title="17. How many people live in your household?"
  x-label="Households"
  y-label="People in Household"
  x-min="0"
  stacked="false"
  source=" CCRPC; U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table S2501"
  type="horizontalBar"></rpc-chart>

### Question 18. How many people live in your household by age?

This question asks respondents about the number of people who live in their
household that are under the age of 16 and how many are age 16 and older. The
options are divided at age 16 to control for the number of household members
eligible to have a driver's license.	According to the responses, there are
approximately 1,022 people age 16 or over and eligible to have a driver's license
among all the respondent households.

<rpc-chart url="18.csv"
  chart-title="18. How many people UNDER the age of 16 live in your household?"
  source=" CCRPC"
  type="pie"></rpc-chart>

<rpc-chart url="18b.csv"
  chart-title="18. How many people OVER the age of 16 live in your household?"
  source=" CCRPC"
  type="pie"></rpc-chart>

### Question 19. How many working motor vehicles are there in your household?

A lack of access to a working motor vehicle increases a person's frequency of
walking, bicycling, and transit use. Zero-vehicle households are
underrepresented among the respondents, which likely results in an undercount of
walking and bicycling frequency. Only about 2 percent of respondents reported
not having access to a working motor vehicle in their household, compared with
about 13 percent among all MPA households [according to the
ACS](https://www.census.gov/acs/www/about/why-we-ask-each-question/vehicles/).

<rpc-chart url="19.csv"
  chart-title="19. How many working motor vehicles are there in your household?"
  x-label="Households"
  x-min="0"
  stacked="false"
  source=" CCRPC; U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table DP04"
  type="horizontalBar"></rpc-chart>

### Question 20. What is your total household income?

On average, PABS respondents have higher incomes than the overall MPA
population. Although the PABS income question differs from the ACS question
about income, we can estimate an average of the annual household income for the
PABS population, based on the response categories, to be approximately $87,846
compared with $46,243 for the overall MPA population according to ACS estimates.  

<rpc-table
  url="20.csv"
  table-title="What is your total household income?"
  table-subtitle="Select an option below to indicate the approximate total annual combined income of all the working adults in your household."
  source=" CCRPC; U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Tables DP03 and S1901"
  text-alignment="l,r"></rpc-table>
