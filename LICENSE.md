Pedestrian and Bicycle Survey Report by the
[Champaign County Regional Planning Commission](https://ccrpc.org/) is licensed
under the terms of the [Creative Commons Attribution 4.0 International
License](https://creativecommons.org/licenses/by/4.0/).
